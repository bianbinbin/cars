This is (will be) a collection of smaller projects that I did, that in some form have something to do with self-driving cars

**Project I - Traffic Sign Recognition** (model ready, fine-tuning needed)

Convolutional Neural Network to classify traffic signs. Using theano and with the goal of providing a flexible interface where the architecture of a CNN can be defined with 1 line and that can directly be used for different problems.

Dataset which was used for testing is the [German Traffic Sign Recognition Benchmark](http://benchmark.ini.rub.de/?section=gtsrb&subsection=news) *(scripts for resizing images and the data are not included)*

right now achieves around 96% accuracy with a very simple model, more fine-tuning and a more complex model needed for better results but both takes too much time on my laptop right now



**Project II - SLAM** (still planning)

SLAM implementation in C++